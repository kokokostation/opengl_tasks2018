#include <Application.hpp>
#include <Mesh.hpp>
#include <ShaderProgram.hpp>

#include <vector>

class Bottle : public Application
{
private:
    const int POLYGON_STEP = 2;

    MeshPtr bottle;
    ShaderProgramPtr shader;

    int polygons;
    float size;

    glm::vec3 r(float u, float v);
    glm::vec3 normal(float u, float v);
    void makeBottle(std::vector<glm::vec3>& points, std::vector<glm::vec3>& normals);

public:
    Bottle() : polygons(100), size(1.) {}

    MeshPtr makeBottle(int polygons_, float size_);
    void handleKey(int key, int scancode, int action, int mods) override;
    void makeScene() override;
    void draw() override;
};