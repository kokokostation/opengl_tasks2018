#include <Application.hpp>
#include <Mesh.hpp>
#include <ShaderProgram.hpp>
#include "Bottle.h"

#include <vector>

glm::vec3 Bottle::r(float u, float v)
{
    float c = (size + cos(v / 2) * sin(u) - sin(v / 2) * sin(2 * u));
    float x = c * cos(v);
    float y = c * sin(v);
    float z = sin(v / 2) * sin(u) + cos(v / 2) * sin(2 * u);
    return glm::vec3(x, y, z);
}

glm::vec3 Bottle::normal(float u, float v)
{
    float c = (cos(v / 2) * cos(u) - 2 * sin(v / 2) * cos(2 * u));
    glm::vec3 ru(c * cos(v), c * sin(v),
                 sin(v / 2) * cos(u) + 2 * cos(v / 2) * cos(2 * u));

    float  c1 = -(sin(v / 2) * sin(u) + cos(v / 2) * sin(2 * u)) / 2,
            c2 = size + cos(v / 2) * sin(u) - sin(v / 2) * sin(2 * u);
    glm::vec3 rv(c1 * cos(v) - c2 * sin(v),
                 c1 * sin(v) + c2 * cos(v),
                 (cos(v / 2) * sin(u) - sin(v / 2) * sin(2 * u)) / 2);

    return glm::normalize(glm::cross(ru, rv));
}

void Bottle::makeBottle(std::vector<glm::vec3>& points, std::vector<glm::vec3>& normals)
{
    float step = 2. * glm::pi<float>() / polygons;

    for(int i = 0; i < polygons; ++i){
        for(int j = 0; j < polygons; ++j)
        {
            float u1 = step * i, v1 = step * j;
            float u2 = u1 + step, v2 = v1 + step;

            float uv_points[4][2] = {{u1, v1}, {u2, v1}, {u1, v2}, {u2, v2}};

            for(int k = 0; k < 2; ++k)
                for(int l = 0; l < 3; ++l)
                {
                    float u = uv_points[k + l][0], v = uv_points[k + l][1];
                    points.push_back(r(u, v));
                    normals.push_back(normal(u, v));
                }
        }
    }
}

MeshPtr Bottle::makeBottle(int polygons_, float size_)
{
    polygons = polygons_;
    size = size_;

    std::vector<glm::vec3> points;
    std::vector<glm::vec3> normals;

    makeBottle(points, normals);

    DataBufferPtr vbuf = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    vbuf->setData(points.size() * sizeof(float) * 3, points.data());

    DataBufferPtr nbuf = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    nbuf->setData(normals.size() * sizeof(float) * 3, normals.data());

    MeshPtr mesh = std::make_shared<Mesh>();
    mesh->setAttribute(0, 3, GL_FLOAT, GL_FALSE, 0, 0, vbuf);
    mesh->setAttribute(1, 3, GL_FLOAT, GL_FALSE, 0, 0, nbuf);

    mesh->setPrimitiveType(GL_TRIANGLES);
    mesh->setVertexCount(points.size());

    return mesh;
}

void Bottle::handleKey(int key, int scancode, int action, int mods)
{
    Application::handleKey(key, scancode, action, mods);

    int delta = 0;

    if(key == GLFW_KEY_KP_SUBTRACT && polygons > 4)
        delta = -1;
    if(key == GLFW_KEY_KP_ADD)
        delta = 1;

    if(delta != 0)
    {
        polygons += delta * POLYGON_STEP;
        makeScene();
    }
}

void Bottle::makeScene()
{
    Application::makeScene();

    _cameraMover = std::make_shared<FreeCameraMover>();

    bottle = makeBottle(polygons, size);

    shader = std::make_shared<ShaderProgram>("494ManakovData/shader.vert", "494ManakovData/shader.frag");
}

void Bottle::draw()
{
    Application::draw();

    int width, height;
    glfwGetFramebufferSize(_window, &width, &height);

    glViewport(0, 0, width, height);

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    shader->use();

    shader->setMat4Uniform("viewMatrix", _camera.viewMatrix);
    shader->setMat4Uniform("projectionMatrix", _camera.projMatrix);

    shader->setMat4Uniform("modelMatrix", bottle->modelMatrix());

    bottle->draw();
}